/**
 * Project: Person view
 * Created by Alex
 * Date: 06-Dec-16
 * Time: 20:46
 */

import javax.swing.*;
import java.awt.event.*;

public class PersonController {

    //The controller interacts with the view and the model
    private PersonModel p_model;
    private PersonView p_view;

    /**Constructor */
    PersonController(PersonView view, PersonModel model) {

        p_model = model;
        p_view = view;

        //Add listeners to the view
        view.addLoadListener(new LoadListener());
        view.addSelectedListener(new SelectedListener());

    }

    //Listener for the LOAD button to load the file
    class LoadListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p_view.noBox = false;
            p_view.p3.remove(p_view.box);
            try{
                p_view.box = new JComboBox((p_model.readFile(p_view.filepath.getText())));
                p_view.p3.add(p_view.box);
                p_view.revalidate(); //oh yeah refreshing
            }catch(NullPointerException ez){
                p_view.noBox = true;
                p_view.revalidate(); //erase the previous combo box
            }
        }
    }

    //Listener for the selection of the person to show a detailed view
    class SelectedListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(p_view.noBox){
                JOptionPane.showMessageDialog(null,"No option chosen!");
                return;
            }

            try{
                String ssn = p_model.getSSN(p_view.box.getSelectedItem().toString());
                String name = p_model.getName(p_view.box.getSelectedItem().toString());
                long age = p_model.getAge(p_view.box.getSelectedItem().toString());
                p_view.message(ssn, name, age);

            }catch(NullPointerException ez){
                JOptionPane.showMessageDialog(null,"No option chosen!");
            }
        }
    }




}
