import javax.swing.*;
import java.io.File;
import java.util.Scanner;

/**
 * Project: Person view
 * Created by Alex
 * Date: 07-Dec-16
 * Time: 08:53
 */

public class PersonModel {

    //Returns the social security number
    public String getSSN(String t){

        String result = "";
        for(int i = 0; i<t.length(); ++i)
            if(t.substring(i,i+1).equals("|"))
                break;
            else
                result += t.substring(i,i+1);
        return result;

    }

    //Computes the age based on the SSN
    public long getAge(String ssn){

        int birthYear = 1900 + Integer.parseInt(ssn.substring(0,2));

        return (2016-birthYear);
    }

    //Returns the name of the person
    public String getName(String t){
        String result = "";

        for(int i = t.indexOf("|")+1; i<t.length(); ++i){
            result += t.charAt(i);
        }
        return result;
    }

    //Opens the filepath and reads from file
    public static String[] readFile(String filePath){

        Scanner x;
        int count = 0;
        try{
            x = new Scanner(new File(filePath));
            while(x.hasNextLine()){
                count++;
                x.nextLine();
            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, "No file detected at given pathway!");
            return null;
        }

        String[] result = new String[count];

        try{
            x = new Scanner(new File(filePath));
            int i = 0;
            while(x.hasNextLine()){
                result[i] = x.nextLine();
                i++;
            }

        }
        catch(Exception e){
            //nothing
        }

        return result;
    }



}
