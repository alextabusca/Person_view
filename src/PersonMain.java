/**
 * Project: Person view
 * Created by Alex
 * Date: 06-Dec-16
 * Time: 15:17
 */

public class PersonMain {
    public static void main(String args[]){
        PersonModel model = new PersonModel();
        PersonView view = new PersonView(model);
        PersonController controller = new PersonController(view, model);
        view.setVisible(true);
    }
}

//C:\Users\Alex Tabusca\IdeaProjects\Person view\src\list.txt  -  Directory for the text file