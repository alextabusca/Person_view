import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.Scanner;
import java.io.File;


/**
 * Project: Person view
 * Created by Alex
 * Date: 07-Dec-16
 * Time: 10:32
 */
class PersonView extends JFrame{

    //Components
    private JPanel p1;
    private JPanel p2;
    public JPanel p3;
    private JPanel p4;
    private JPanel pFinal;
    private JLabel file_label = new JLabel("Filepath: ");
    public JTextField filepath = new JTextField(10);
    private JButton load = new JButton("Load file");
    private JLabel contents = new JLabel("Content of the file");
    private JButton selected = new JButton("Show selected");
    public JComboBox box = new JComboBox();
    public boolean noBox = false;
    private PersonModel p_model;

    PersonView(PersonModel model){

        //Initialize the model
        p_model = model;

        p1 = new JPanel();
        p1.setLayout(new FlowLayout());
        p1.add(file_label);
        p1.add(filepath);
        p1.add(load);

        p2 = new JPanel();
        p2.setLayout(new FlowLayout());
        p2.add(contents);

        p3 = new JPanel();
        p3.setLayout(new FlowLayout());

        p4 = new JPanel();
        p4.setLayout(new FlowLayout());
        p4.add(selected);

        pFinal = new JPanel();
        pFinal.setLayout(new BoxLayout(pFinal,BoxLayout.PAGE_AXIS));
        pFinal.add(p1);
        pFinal.add(p2);
        pFinal.add(p3);
        pFinal.add(p4);

        this.setContentPane(pFinal);
        this.setTitle("Person View");
        this.pack();
        setSize(440,180);
        setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void message(String SSN, String name, long age){

        Object[] options = {"Cancel", "Ok"};
        int n = JOptionPane.showOptionDialog(null,
                "SSN: " + SSN + "\nName: " + name + "\nAge: " + age,
                "Person Info",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.PLAIN_MESSAGE,
                null,     //do not use a custom Icon
                options,  //the titles of buttons
                options[1]); //default button title
    }

    void addLoadListener(ActionListener ll) {
        load.addActionListener(ll);
    }

    void addSelectedListener(ActionListener sl) {
        selected.addActionListener(sl);
    }


}
